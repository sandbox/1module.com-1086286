<?php 

/**
 * Form to change 1 Flash Gallery settings.
 *
 * @ingroup forms
 */
function one_flash_gallery_settings_form() {
drupal_add_js(drupal_get_path('module', 'one_flash_gallery') .'/js/copy.js', 'file');
drupal_add_js(drupal_get_path('module', 'one_flash_gallery') .'/js/ZeroClipboard.js', 'file');
  $form = array();

  $form['one_flash_gallery_per_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Galleries per page'),
    '#description' => t('Set the number of gallery items per page'),
    '#default_value' => variable_get('one_flash_gallery_per_page', 10),
  );

  $form['one_flash_gallery_images_per_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Images per page'),
	'#description' => t('Set the number of image items per page'),
    '#default_value' => variable_get('one_flash_gallery_images_per_page', 25),
  );
  
  $output = '<div id="fgallery_settings" class="fgallery_box">';
  $output .= '<h2>'.t('Server Settings').'</h2>';
  $output .= '<p><span class="lbl">'.t('Server Name:').'</span>'.$_SERVER['SERVER_NAME'].'</p>';
  $output .= '<p><span class="lbl">'.t('Document Root:').'</span>'.$_SERVER['DOCUMENT_ROOT'].'</p>';
  $output .= '<p><span class="lbl">'.t('Web server:').'</span>'.$_SERVER['SERVER_SOFTWARE'].'</p>';
  $output .= '<p><span class="lbl">'.t('Host:').'</span>'.$_SERVER['HTTP_HOST'].'</p>';
  $output .= '<p><span class="lbl">'.t('Client Agent:').'</span>'.$_SERVER['HTTP_USER_AGENT'].'</p>';
  $output .= '<p><span class="lbl">'.t('Max size of uploaded file:').'</span>'.ini_get('upload_max_filesize').'</p>';
   if (ini_get('allow_url_fopen') != 1 ) {
	   $output .= '<p class="fgallery_error">'.t("Set allow_url_fopen to 'On' in php.ini").'</p>';
   }
   $output .= '</div>';
   $output .= '<button id="copypref" rel="'.base_path().drupal_get_path('module','one_flash_gallery').'/swf/ZeroClipboard.swf">'.t('Copy settings to clipboard').'</button>';
  
  $form['#suffix'] = $output;
  
  return system_settings_form($form);
}

/**
 * List all galleries.
 */
function one_flash_gallery_administration() {
  drupal_add_css(drupal_get_path('module', 'one_flash_gallery') .'/css/fgallery.css','module','all',FALSE);
  $output = '';

  $header = array();
  $tbl_columns = one_flash_gallery_table_header();
  foreach ($tbl_columns as $column) {
    $header[] = $column['cell'];
  }
  $order = substr(tablesort_sql($header), 10);
  if (empty($_REQUEST['order'])) {
    $order = 'gall_order';
  }

  $nids = array();
  // Display 10 (default) products per page.
  $per_page = variable_get('one_flash_gallery_per_page', 10);
  $result = pager_query("SELECT * FROM {fgallery_galleries} ORDER BY ". $order, $per_page, 0, NULL);
  while ($gallery = db_fetch_object($result)) {
    $nids[] = $gallery->gall_id;
  }
	
  $table = tapir_get_table('one_flash_gallery_table', $nids);
  $output .= '<div class="fgallery_actions">';
  if (!$parent) $output .= l(t('Create new gallery'),'admin/1-flash-gallery/galleries/add',array('attributes'=>array('class'=>'fgallery_action')));
  $output .= l(t('Upload images'),'admin/1-flash-gallery/upload',array('attributes'=>array('class'=>'fgallery_action')));
  $output .= '</div><div class="clear"></div>';
  // Display the product table and pager if necessary.
  $output .= drupal_render($table) . theme('pager');
  
  return $output;
}

/**
 * List all galleries.
 */
function one_flash_gallery_images_admin($admin_callback = NULL, $folder = NULL) {
drupal_add_js(drupal_get_path('module', 'one_flash_gallery') .'/js/fgallery_images.js', 'file');
drupal_add_css(drupal_get_path('module', 'one_flash_gallery') .'/css/fgallery.css','module','all',FALSE);
  $output = '';

  $header = array();
  $tbl_columns = one_flash_gallery_images_table_header();
  foreach ($tbl_columns as $column) {
    $header[] = $column['cell'];
  }
  $order = substr(tablesort_sql($header), 25);
  if (empty($_REQUEST['order'])) {
    $order = 'img_date DESC';
  }
  if (!empty($folder)) {
	$parent = $folder->img_id;
 }  else {
	$parent = 0;
 }
  $nids = array();
  // Display 25 (default) galleries per page.
  $per_page = variable_get('one_flash_gallery_images_per_page', 25);
  $result = pager_query("SELECT * FROM {fgallery_images} WHERE img_parent = ".$parent." AND img_vs_folder IN (0,1) ORDER BY img_vs_folder DESC,". $order, $per_page, 0, NULL);
  while ($image = db_fetch_object($result)) {
    $nids[] = $image->img_id;
	$values[$image->img_id] = $image->img_caption;
  }
  
  $table = tapir_get_table('one_flash_gallery_images_table', $nids);
  $output = drupal_get_form('one_flash_gallery_image_massedit_form', $values);
  $output .= '<div class="fgallery_actions">';
  if (!$parent) $output .= l(t('Create new folder'),'admin/1-flash-gallery/folder/1',array('attributes'=>array('class'=>'fgallery_action')));
  $output .= l(t('Upload images'),'admin/1-flash-gallery/upload',array('attributes'=>array('class'=>'fgallery_action')));
  $output .= '</div><div class="clear"></div>';
  // Display the galleries table and pager if necessary.
  $output .= drupal_render($table) . theme('pager');
  
  return $output;
}

function one_flash_gallery_addimages($gallery, $folder = NULL) {
	drupal_add_js(drupal_get_path('module', 'one_flash_gallery') .'/js/fgallery_images.js', 'file');
drupal_add_css(drupal_get_path('module', 'one_flash_gallery') .'/css/fgallery.css','module','all',FALSE);
	$output = '';
	$header = array();
	  $tbl_columns = one_flash_gallery_images_table_header();
	  foreach ($tbl_columns as $column) {
		$header[] = $column['cell'];
	  }
	  $order = substr(tablesort_sql($header), 10);
	  if (empty($_REQUEST['order'])) {
		$order = 'img_date DESC';
	  }
	  if (!empty($folder)) {
		$parent = $folder->img_id;
	 }  else {
		$parent = 0;
	 }
	  $nids = array();
	  // Display 25 (default) galleries per page.
	  $per_page = variable_get('one_flash_gallery_images_per_page', 25);
	  $result = pager_query("SELECT * FROM {fgallery_images} WHERE img_id NOT IN (SELECT img_id FROM {fgallery_gallery_images} WHERE gall_id = ".$gallery->gall_id.") AND img_parent = ".$parent." AND img_vs_folder IN (0,1) ORDER BY img_vs_folder DESC,". $order, $per_page, 0, NULL);
	  while ($image = db_fetch_object($result)) {
		$nids[] = $image->img_id;
		$values[$image->img_id] = $image->img_caption;
	  }
	  
	  $table = tapir_get_table('one_flash_gallery_images_table', $nids);
	  $output = drupal_get_form('one_flash_gallery_addimages_form', $values, $gallery);
	  // Display the galleries table and pager if necessary.
	  $output .= drupal_render($table) . theme('pager');
	  
	  return $output;
}

function one_flash_gallery_list_images($gallery, $folder = NULL) {
	drupal_add_js(drupal_get_path('module', 'one_flash_gallery') .'/js/fgallery_images.js', 'file');
drupal_add_css(drupal_get_path('module', 'one_flash_gallery') .'/css/fgallery.css','module','all',FALSE);
	$output = '';
	$header = array();
	  $tbl_columns = one_flash_gallery_images_table_header();
	  foreach ($tbl_columns as $column) {
		$header[] = $column['cell'];
	  }
	  $order = substr(tablesort_sql($header), 10);
	  if (empty($_REQUEST['order'])) {
		$order = 'a.img_date DESC';
	  }
	  if (!empty($folder)) {
		$parent = $folder->img_id;
	 }  else {
		$parent = 0;
	 }
	  $nids = array();
	  // Display 25 (default) galleries per page.
	  $per_page = variable_get('one_flash_gallery_images_per_page', 25);
	  $result = pager_query("SELECT a.* FROM {fgallery_images} as a LEFT JOIN {fgallery_gallery_images} as b ON (a.img_id = b.img_id) WHERE b.gall_id =".$gallery->gall_id." AND b.gall_folder = ".$parent." AND a.img_vs_folder IN (0,2) ORDER BY a.img_vs_folder DESC, ".$order, $per_page, 0, NULL);
	  while ($image = db_fetch_object($result)) {
		$nids[] = $image->img_id;
		$values[$image->img_id] = $image->img_caption;
	  }
	    
	  $table = tapir_get_table('one_flash_gallery_images_table', $nids, $gallery->gall_id);
	  $output = drupal_get_form('one_flash_gallery_list_images_form', $values, $gallery);
	  $output .= '<div class="fgallery_actions">';
		  if (!$parent) $output .= l(t('Create new album'),'admin/1-flash-gallery/galleries/'.$gallery->gall_id.'/album',array('attributes'=>array('class'=>'fgallery_action')));
		  $output .= '</div><div class="clear"></div>';
	  // Display the galleries table and pager if necessary.
	  drupal_set_title(t('Images from ').l($gallery->gall_name,'admin/1-flash-gallery/galleries/'.$gallery->gall_id.'/edit'));
	  $output .= drupal_render($table) . theme('pager');
	  
	  return $output;
}

function one_flash_gallery_addimages_form($from_state, $nids, $gallery) {
  $form = array();
  $form['images'] = array('#type' => 'checkboxes', '#options' => $nids);
  $form['gall_id'] = array('#type' => 'hidden', '#default_value' => $gallery->gall_id);
  $form['buttons']['submit'] = array(
	'#type'=> 'submit',
	'#value' => t('Add to gallery'),
	'#weight' => 50,
  );
  return $form;
}

function one_flash_gallery_addimages_form_submit($form, &$form_state) {
	if (!empty($form_state['values']['images'])){
		$gall_id = $form_state['values']['gall_id'];
					$ids = $form_state['values']['images'];
					foreach ($ids as $key=>$value) {
						if ($value != 0){
							$image = one_flash_gallery_image_load($value);
								if ($image->img_vs_folder == 1) {
									db_query("INSERT INTO {fgallery_images} (img_caption, img_vs_folder, img_date) VALUES ('%s', %d, NOW())", $image->img_caption, 2);
									$id = db_last_insert_id('{fgallery_images}','img_id');
									db_query("INSERT INTO {fgallery_gallery_images} (img_id, gall_id, gall_folder, img_order) VALUES (%d,%d,%d)", $id, $gall_id, 0, 0);
									$images = one_flash_gallery_get_images_from_folder($image->img_id);
									foreach ($images as $row) {
										db_query("INSERT INTO {fgallery_gallery_images} (img_id, gall_id, gall_folder, img_order) VALUES (%d, %d, %d, %d)", $row->img_id, $gall_id, $id, 0);
									}
								} elseif ($image->img_vs_folder == 0) {
									db_query("INSERT INTO {fgallery_gallery_images} (img_id, gall_id, img_order) VALUES (%d,%d,%d)", $value, $gall_id, 0);
								}
							}
				}
			}
	drupal_goto('admin/1-flash-gallery/galleries/'.$form_state['values']['gall_id'].'/edit');
}

function one_flash_gallery_list_images_form($from_state, $nids, $gallery) {
   $options = array(
	t('Move to album') => one_flash_gallery_get_folders(2, $gallery->gall_id),
  );
  $form = array();
  $form['image_action'] = array(
	'#type' => 'select',
	'#options' => $options,
	'#default_value' => '-',
  );
  $form['gall_id'] = array('#type' => 'hidden', '#default_value' => $gallery->gall_id);
  $form['images'] = array('#type' => 'checkboxes', '#options' => $nids);
  $form['buttons']['submit'] = array(
	'#type'=> 'submit',
	'#value' => t('Go'),
	'#weight' => 50,
  );
  return $form;
}

function one_flash_gallery_list_images_form_submit($form, &$form_state) {
if (!empty($form_state['values']['images'])){
	$gall_id = $form_state['values']['gall_id'];
		$ids = $form_state['values']['images'];
		$folder = $form_state['values']['image_action'];
		foreach ($ids as $key=>$value) {
			if ($value != 0) {
				db_query("UPDATE {fgallery_gallery_images} SET gall_folder = %d WHERE img_id = %d AND gall_id = %d", $folder, $value, $gall_id); 
				}
			}
		}

	drupal_goto('admin/1-flash-gallery/galleries/'.$gall_id.'/edit/listimages');
}

function one_flash_gallery_image_massedit_form($from_state, $nids) {
   $options = array(
	t('Actions') => array(
			'-' => t('Choose action'),
			'-1' => t('Make new gallery from selected'),
			'-2' => t('Delete selected'),
		),
	t('Move to folder') => one_flash_gallery_get_folders(),
	t('Add selected to') => one_flash_gallery_get_galleries(),
  );
  $form = array();
  $form['image_action'] = array(
	'#type' => 'select',
	'#options' => $options,
	'#default_value' => '-',
  );
  $form['images'] = array('#type' => 'checkboxes', '#options' => $nids);
  $form['buttons']['submit'] = array(
	'#type'=> 'submit',
	'#value' => t('Go'),
	'#weight' => 50,
  );
  return $form;
}

function one_flash_gallery_image_massedit_form_submit($form, &$form_state) {
    if (is_numeric($form_state['values']['image_action'])){
		switch ($form_state['values']['image_action']) {
			case '-2':
				if (!empty($form_state['values']['images'])){
					$ids = $form_state['values']['images'];
					foreach ($ids as $id) {
						one_flash_gallery_delete_image($id);
					}
				}
				drupal_goto('admin/1-flash-gallery/images');
				break;
			case '-1':	
			global $user;
				if (!empty($form_state['values']['images'])) {
					$ids = $form_state['values']['images'];
					$user_id = $user->uid;
					$max = db_fetch_object(db_query("SELECT MAX(gall_order) as value FROM {fgallery_galleries}"));
					$insert_id = db_query("INSERT INTO {fgallery_galleries} (gall_name, gall_createddate, gall_createdby, gall_published, gall_order) VALUES ('%s',NOW(),%d,%d,%d)", t('New gallery'),$user_id, 1, $max->value+1);
					$gall_id = db_last_insert_id('{fgallery_galleries}','gall_id');
					foreach ($ids as $key=>$value) {
					if ($value != 0) {
							$image = one_flash_gallery_image_load($value);
							if ($image->img_vs_folder == 1) {
								$images = one_flash_gallery_get_images_from_folder($image->img_id);
								foreach ($images as $row) {
									db_query("INSERT INTO {fgallery_gallery_images} (img_id, gall_id, img_order) VALUES (%d,%d,%d)", $row->img_id, $gall_id, 0);
								}
							} elseif ($image->img_vs_folder == 0) {
								db_query("INSERT INTO {fgallery_gallery_images} (img_id, gall_id, img_order) VALUES (%d,%d,%d)", $image->img_id, $gall_id, 0);
							}
						}
					}
				drupal_goto('admin/1-flash-gallery/galleries/'.$gall_id.'/edit');
				}
				break;
			default:
				if (!empty($form_state['values']['images'])) {
					$ids = $form_state['values']['images'];
					foreach ($ids as $key=>$value) {
						if ($value == 0) unset($ids[$key]);
					}
					$ids_string = implode(",",$ids);
					db_query("UPDATE {fgallery_images} SET img_parent = %d WHERE img_id IN (%s) AND img_vs_folder = %d", $form_state['values']['image_action'], $ids_string, 0 );
				}
				drupal_goto('admin/1-flash-gallery/images');
				break;
		}
	} else {
			$gall_id = str_replace('gall_','',$form_state['values']['image_action']);
			if (is_numeric($gall_id)) {
					if (!empty($form_state['values']['images'])) {
							$ids = $form_state['values']['images'];
							foreach ($ids as $key=>$value) {
							if ($value != 0){
								$image = one_flash_gallery_image_load($value);
									if ($image->img_vs_folder == 1) {
										db_query("INSERT INTO {fgallery_images} (img_caption, img_vs_folder, img_date) VALUES ('%s', %d, NOW())", $image->img_caption, 2);
										$par_id = db_last_insert_id('{fgallery_images}','img_id');
										db_query("INSERT INTO {fgallery_gallery_images} (img_id, gall_id, gall_folder, img_order) VALUES (%d,%d,%d,%d)", $par_id, $gall_id, 0, 0);
										$images = one_flash_gallery_get_images_from_folder($image->img_id);
										foreach ($images as $row) {
											db_query("INSERT INTO {fgallery_gallery_images} (img_id, gall_id, gall_folder, img_order) VALUES (%d,%d,%d,%d)", $row->img_id, $gall_id, $par_id, 0);
										}
									} elseif ($image->img_vs_folder == 0) {
										db_query("INSERT INTO {fgallery_gallery_images} (img_id, gall_id, img_order) VALUES (%d,%d,%d)", $value, $gall_id, 0);
									}
								}
							}
						drupal_goto('admin/1-flash-gallery/galleries/'.$gall_id.'/edit');
						}
			} else {
				drupal_goto('admin/1-flash-gallery/images');
			}
	}
}

function one_flash_gallery_image_add_form($form_state) {
	$form = array();
	$form['img_caption'] = array(
		'#type' => 'textfield',
		'#title' => t('Name'),
		'#required' => TRUE,
	);
	$form['img_vs_folder'] = array(
		'#type' => 'hidden',
		'#default_value' => 1,
	);
	$form['buttons']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
	);
	return $form;
}

function one_flash_gallery_image_add_form_submit($form, &$form_state) {
	$name = htmlentities($form_state['values']['img_caption'], ENT_NOQUOTES, "UTF-8");
	$type = $form_state['values']['img_vs_folder'];
	if (!db_query("INSERT INTO {fgallery_images} (img_caption, img_date, img_vs_folder) VALUES('%s', NOW(), %d)", $name, $type)) {
		drupal_set_error(t('Error occured while saving. Please try again'));
	} else {
		drupal_set_message(t(sprintf('Folder %s successfully saved', $name)));
	}
	$form_state['redirect'] = 'admin/1-flash-gallery/images';
}

function one_flash_gallery_album_form($form_state, $gallery) {
	$form = array();
	$form['img_caption'] = array(
		'#type' => 'textfield',
		'#title' => t('Name'),
		'#required' => TRUE,
	);
	$form['img_vs_folder'] = array(
		'#type' => 'hidden',
		'#default_value' => 2,
	);
	$form['gall_id'] = array('#type'=>'hidden', '#default_value' => $gallery->gall_id);
	$form['buttons']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
	);
	return $form;
}

function one_flash_gallery_album_form_submit($form, &$form_state) {
	$gall_id = $form_state['values']['gall_id'];
	$name = htmlentities($form_state['values']['img_caption'], ENT_NOQUOTES, "UTF-8");
	$type = $form_state['values']['img_vs_folder'];
	if (!db_query("INSERT INTO {fgallery_images} (img_caption, img_date, img_vs_folder) VALUES('%s', NOW(), %d)", $name, 2)) {
		drupal_set_error(t('Error occured while saving. Please try again'));
	} else {
		$img_id = db_last_insert_id('{fgallery_images}','img_id');
		db_query("INSERT INTO {fgallery_gallery_images} (img_id, gall_id, gall_folder, img_order) VALUES (%d, %d, %d, %d)", $img_id, $gall_id, 0, 0);
		drupal_set_message(t(sprintf('Album %s successfully saved', $name)));
	}
	drupal_goto('admin/1-flash-gallery/galleries/'.$gall_id.'/edit/listimages');
}

function one_flash_gallery_image_edit_form($form_state, $image) {
	$form = array();
	$folder = false;
	if ($image->img_vs_folder > 0) $folder = true;
	
	$form['img_caption'] = array(
		'#type' => 'textfield', 
		'#title' => $folder ? t('Folder Name') : t('Image Caption'), 
		'#default_value' => $image->img_caption,
	);
	if (!$folder) {
		$form['img_description'] = array(
			'#type' => 'textarea', 
			'#title' => t('Image description'), 
			'#default_value' => $image->img_description,
			'#suffix' => '<div id="preview">'.theme('imagecache', 'fgallery_view', $image->img_path, $image->img_description, $image->img_caption).'</div>',
		);
	}
	$form['img_id'] = array(
		'#type' => 'hidden',
		'#value' => $image->img_id,
	);
	$form['buttons']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
	);
	
	return $form;
}

function one_flash_gallery_image_edit_form_submit($form, &$form_state) {
	$name = htmlentities($form_state['values']['img_caption'], ENT_NOQUOTES, "UTF-8");
    $desc = htmlentities($form_state['values']['img_description'], ENT_NOQUOTES, "UTF-8");
	$id = $form_state['values']['img_id'];
	if (!db_query("UPDATE {fgallery_images} SET img_caption = '%s', img_description='%s' WHERE img_id = %d", $name, $desc, $id)) {
		drupal_set_error(t('Error occured while saving. Please try again'));
	} else {
		drupal_set_message(t('Item successfully saved'));
	}
}

function one_flash_gallery_delete_form($form_state, $gallery) {
	if (empty($gallery)) {
		drupal_set_message(t('Unknown Gallery.'), 'error');
		drupal_goto('admin/1-flash-gallery/galleries');	
	}
	$form =array();
	$form['gall_id'] = array('#type' => 'value', '#value' => $gallery->gall_id);
	  return confirm_form(
		$form,
		t('Are you sure you want to delete !gallery?',
		  array('!gallery' => $gallery->gall_name)
		),
		'admin/1-flash-gallery/galleries',
		t('This action cannot be undone.'),
		t('Delete'),  t('Cancel')
	  );
}

function one_flash_gallery_delete_form_submit($form, &$form_state) {
  $id = $form_state['values']['gall_id'];
  db_query("DELETE FROM {fgallery_gallery_images} WHERE gall_id = %d", $id);
  db_query("DELETE FROM {fgallery_gallery_settings} WHERE gall_id = %d", $id);
  db_query("DELETE FROM {fgallery_galleries} WHERE gall_id = %d", $id);
  drupal_set_message(t('Gallery was successfully deleted'));
  $form_state['redirect'] = 'admin/1-flash-gallery/galleries';
}

function one_flash_gallery_image_delete_form($form_state, $image) {
	if (empty($image)) {
		drupal_set_message(t('Unknown Image.'), 'error');
		drupal_goto('admin/1-flash-gallery/images');
	  }
	  $form = array();
	  $form['img_id'] = array('#type' => 'value', '#value' => $image->img_id);
	  return confirm_form(
		$form,
		t('Are you sure you want to delete the !image image from the list?',
		  array('!image' => $image->img_caption)
		),
		'admin/1-flash-gallery/images',
		t('This action cannot be undone.'),
		t('Delete'),  t('Cancel')
	  );
}

function one_flash_gallery_image_delete_form_submit($form, &$form_state) {
  $id = $form_state['values']['img_id'];
  one_flash_gallery_delete_image($id);
  drupal_set_message(t('The image has been deleted.'));
  $form_state['redirect'] = 'admin/1-flash-gallery/images';
}

function one_flash_gallery_delete_image($id) {
	$image = one_flash_gallery_image_load($id);
	$folder = $image->img_vs_folder;
	$path = $image->img_path;
	if ($folder == 2) {
		db_query("DELETE FROM {fgallery_gallery_images} WHERE gall_folder = %d", $id);
	}
  db_query("DELETE FROM {fgallery_gallery_images} WHERE img_id = %d", $id);
  if ($folder == 1) {
	db_query("UPDATE {fgallery_images} SET img_parent = 0 WHERE img_parent = %d",$id);
	db_query("DELETE FROM {fgallery_images} WHERE img_id = %d",$id);
  } elseif ($folder == 0) {
	file_delete($path);
	db_query("DELETE FROM {fgallery_images} WHERE img_id = %d",$id);
  }	
}

function one_flash_gallery_remove_image($gallery, $image) {
	db_query("DELETE FROM {fgallery_gallery_images} WHERE img_id = %d AND gall_id = %d", $image->img_id, $gallery->gall_id);
	drupal_set_message(t(sprintf('Image %s was removed from %s gallery', $image->img_caption, $gallery->gall_name)));
	drupal_goto('admin/1-flash-gallery/galleries/'.$gallery->gall_id.'/edit/listimages');
}

function one_flash_gallery_edit($gallery) {
global $base_url;
drupal_add_js(drupal_get_path('module', 'one_flash_gallery') .'/js/copy.js', 'file');
drupal_add_js(drupal_get_path('module', 'one_flash_gallery') .'/js/ZeroClipboard.js', 'file');
	if (empty($gallery)) {
		drupal_set_message(t('Unknown Gallery.'), 'error');
		drupal_goto('admin/1-flash-gallery/galleries');
	}
	if ($gallery->gall_id > 0) {
		$output = '<div id="shortcode_view">'.one_flash_gallery_do_shortcode($gallery).'</div>';
		$output .= '<button id="shortcode" rel="'.base_path().drupal_get_path('module','one_flash_gallery').'/swf/ZeroClipboard.swf">'.t('Copy').'</button>';
	}
	$output .= drupal_get_form('one_flash_gallery_edit_form', $gallery);
	$type = one_flash_gallery_get_flash_type($gallery);
	$params = array('width'=> $gallery->gall_width, 'height'=>$gallery->gall_height, 'bgcolor' => $gallery->gall_bgcolor, 'allowFullScreen' => 'true', 'wmode'=>'transparent');
	$vars = array('settings' => base_path().'1-flash-gallery/galleries/'.$gallery->gall_id.'/settings', 'images' => base_path().'1-flash-gallery/galleries/'.$gallery->gall_id.'/images');
	$output .= theme("swfobject_api", base_path().drupal_get_path('module','one_flash_gallery').'/swf/'.$type.'.swf', $params, $vars, 'flashcontent');
	return $output;
}

function one_flash_gallery_edit_form($form_state, $gallery = NULL) {
global $base_url;
		drupal_set_title(t('Edit ').$gallery->gall_name);
	if (empty($gallery)) {
		$gallery->gall_type = 3;
		$gallery->gall_width = 450;
		$gallery->gall_height = 385;
		$gallery->gall_bgcolor = '#ffffff';
		drupal_set_title(t('Add New Gallery'));
	} else {
		$settings_xml = simplexml_load_file($base_url.'/1-flash-gallery/galleries/'.$gallery->gall_id.'/settings');
	}
	if (module_exists('jquery_ui')) jquery_ui_add(array('ui.tabs','ui.slider'));
	drupal_add_js('misc/farbtastic/farbtastic.js'); 
	drupal_add_css('misc/farbtastic/farbtastic.css');
	drupal_add_css(drupal_get_path('module','one_flash_gallery').'/css/ui.base.css');
	drupal_add_css(drupal_get_path('module','one_flash_gallery').'/css/configurator.css');
	drupal_add_js(drupal_get_path('module', 'one_flash_gallery') .'/js/configurator.js', 'file');
	
	
	$params_xml = simplexml_load_file(drupal_get_path('module','one_flash_gallery') . '/xml/params_'.$gallery->gall_type.'.xml');
		  // Preparing jQuery UI tabs
	  $tab_controls = '<ul>';
		$tab_controls .= '<li><a href="#sc-group-main">' . t('General') . '</a></li>';
	  foreach ($params_xml->params->group as $g) {
		 $tab_controls .= '<li><a href="#sc-group-' . $g['name'] . '">' . t((string)$g['title']) . '</a></li>';
		}
	  $tab_controls .= '</ul>';
  
	$form = array();
	$form['sc-group-main'] = array(
		'#type' => 'fieldset',
		'#title' => t('General'),
		'#collapsed' => FALSE,
		'#attributes'=>array('id'=>'sc-group-main'),
		'#prefix'=> $tab_controls,
	);
	$form['sc-group-main']['gall_id'] = array(
		'#type' => 'hidden',
		'#default_value' => $gallery->gall_id,
	);
	$form['sc-group-main']['gall_name'] = array(
		'#type' => 'textarea',
		'#title' => t('Title'),
		'#default_value' => $gallery->gall_name,
		'#resizable' => FALSE,
		'#required' => TRUE,
		'#cols' => 30,
		'#rows' => 5,
	);
	$form['sc-group-main']['gall_description'] = array(
		'#type' => 'textarea',
		'#title' => t('Description'),
		'#default_value' => $gallery->gall_description,
		'#resizable' => FALSE,
		'#cols' => 30,
		'#rows' => 5,
	);
	$form['sc-group-main']['gall_source'] = array(
		'#type' => 'select',
		'#options' => array('local' => t('Local')),
		'#attributes' => array('class' => 'fgallery_hide'),
	);
	$form['sc-group-main']['gall_width'] = array(
		'#type' => 'textfield',
		'#title' => t('Width'),
		'#default_value' => $gallery->gall_width,
	);
	$form['sc-group-main']['gall_height'] = array(
		'#type' => 'textfield',
		'#title' => t('Height'),
		'#default_value' => $gallery->gall_height,
	);
	$form['sc-group-main']['gall_bgcolor'] = array(
		'#type' => 'textfield',
		'#title' => t('Background Color'),
		'#default_value' => '#'.$gallery->gall_bgcolor,
		'#attributes'=> array('class'=>'sc-color-val'),
	);
	$form['sc-group-main']['gall_type'] = array(
		'#type' => 'select',
		'#options' => array('1' => t('Acosta'), '2' => t('Airion'), '3' => t('Arai'),
							'4' => t('Pax'), '5' => t('Pazin'), '6' => t('Postma')),
		'#title'=> t('Gallery Type'),
		'#default_value' => $gallery->gall_type,
	);
	
	foreach ($params_xml->params->group as $g) {
		$form['sc-group-'.$g['name']] = array(
			'#type' => 'fieldset',
			'#title' => $g['title'],
			'#collapsed' => FALSE,
			'#attributes'=>array('id'=>'sc-group-'.$g['name']),
		);
		    foreach ($g->p as $p) {
			$add = '';
			if ($p['control'] == 'color') $add = 'picker';
			  if (function_exists($fn = 'sc_controls_' . $p['control'].$add)) {
				$p['element_name'] = 'sc_' . $g['name'] . '__' . $p['name'];
				$p['default'] = (string) $settings_xml->$g['name']->$p['name'];
				$form['sc-group-'.$g['name']][(string)$p['element_name']] = $fn($p);
			  }
			}
	}
	$form['buttons']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
	);
	return $form;
}

function one_flash_gallery_prepare_settings($data) {
	$new = one_flash_gallery_default_settings();
	
	foreach ($new as $key=>$value) {
		if ($data[$key] != '') {
			$new[$key] = (string)$data[$key];
		}
		if ((string)$data[$key] == 'true') {
			$new[$key] = 1;
		} 
	}
	return $new;
}

function one_flash_gallery_save_settings($gall_id, $data) {
	$result = db_result(db_query("SELECT gall_id FROM {fgallery_gallery_settings} WHERE gall_id = %d",$gall_id));
	if ($result > 0) {
		if (!db_query("UPDATE {fgallery_gallery_settings} SET value = '%s' WHERE gall_id = %d", serialize($data), $gall_id)){
			return false;
		}
	} else {
		if (!db_query("INSERT INTO {fgallery_gallery_settings} (gall_id, value) VALUES (%d, '%s')", $gall_id, serialize($data))) {
			return false;
		}
	}
	return true;
}

function one_flash_gallery_edit_form_submit($form, &$form_state) {
	$values = $form_state['values'];
	$gall_id = $values['gall_id'];
	if ($gall_id == '') {
		global $user;
		db_query("INSERT INTO {fgallery_galleries} (gall_name, gall_description, gall_createddate, gall_createdby, gall_width, gall_height, gall_bgcolor, gall_type) VALUES ('%s', '%s', NOW(), %d, %d, %d, '%s', %d)",$values['gall_name'], $values['gall_description'], $user->uid, $values['gall_width'], $values['gall_height'], str_replace('#','',$values['gall_bgcolor']), $values['gall_type']);
		$gall_id = db_last_insert_id('{fgallery_galleries}','gall_id');
	} else {
		if (!db_query("UPDATE {fgallery_galleries} SET gall_name ='%s', gall_description = '%s', gall_width = %d, gall_height = %d, gall_bgcolor = '%s', gall_type = %d WHERE gall_id = %d", $values['gall_name'], $values['gall_description'], $values['gall_width'], $values['gall_height'], str_replace('#','',$values['gall_bgcolor']), $values['gall_type'], $gall_id)) {
			drupal_set_message(t('Error occured while saving'));
		}
	}
	$to_store = one_flash_gallery_prepare_settings($values);
	if (one_flash_gallery_save_settings($gall_id, $to_store)) {
		drupal_set_message(t('Gallery settings successfully saved'));
	} else {
		drupal_set_message(t('Error occured while saving'));
	}
	drupal_goto('admin/1-flash-gallery/galleries/'.$gall_id.'/edit');
}

/**
 * Element text
 */
function sc_controls_text($p) {
	$field = array(
		'#type' => 'textfield',
		'#title'=> (string)$p['title'],
		'#default_value' => (string)$p['default'],
		'#attributes' => array('class' =>'form-text '.(string)$p['class']),
	);
  return $field;
}

/**
 * Element - checkbox.
 */
function sc_controls_checkbox($p) {
if ((string)$p['default'] == $p['values']) {
  $field = array(
	'#type' => 'checkbox',
	'#title' => (string)$p['title'],
	'#default_value' => (string)$p['default'],
	'#return_value' => $p['values'],
	'#attributes' => array('class'=>'form-checkbox'),
  );
} else {
  $field = array(
	'#type' => 'checkbox',
	'#title' => (string)$p['title'],
	'#return_value' => $p['values'],
	'#attributes' => array('class'=>'form-checkbox'),
  );
}
  return $field;
}

/**
 * Element - select.
 */
function sc_controls_select($p) {
  $options_raw = explode(',', $p['values']);
  $options = array();
  foreach ($options_raw as $key=>$value) {
	$options[$value] = $value;
  }
  $field = array(
	'#type' => 'select',
	'#options' => $options,
	'#defalut_value' => $p['default'],
	'#attributes' => array('class' => 'form-select'),
	'#title' => (string) $p['title'],
  );
  return $field;
}

/**
 * Element - slider (jQuery UI slider).
 */
function sc_controls_slider($p) {
  $values = explode(':', $p['values']); // format "1..5:1"
  $range = $values[0];
  $range = explode('..', $range);
  $min = $range[0];
  $max = $range[1];

  $step = $values[1];

  // Validate values
  if (!is_numeric($step) || !is_numeric($min) || !is_numeric($max)) {
    return '<div class="form-item ' . $p['zebra'] . '" >Parse error</div>';
  }
  
  $field = array(
	'#type' => 'textfield',
	'#title' => (string)$p['title'],
	'#default_value' => $p['default'],
	'#maxlength' => 3,
	'#size' => 3,
	'#attributes' => array('class' => 'sc-slider-val form-text', 'min' => $min, 'max' => $max, 'step' => $step, 'readonly' => 'readonly'),
  );
  
  return $field;
}

/**
 * Form element - color picker (Farbtastic).
 */
function sc_controls_colorpicker($p) {
	$field = array(
		'#title' => (string) $p['title'],
		'#type' => 'textfield',
		'#default_value' => str_replace('0x','#',(string)$p['default']),
		'#attributes' => array('class' => 'sc-color-val'),
	);
	
  return $field;
}
