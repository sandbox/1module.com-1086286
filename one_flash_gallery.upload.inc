<?php 
function one_flash_gallery_images_upload() {
	return drupal_get_form('one_flash_gallery_images_upload_local_form');
} 

function one_flash_gallery_images_upload_local() {
	return drupal_get_form('one_flash_gallery_images_upload_local_form');
}

function one_flash_gallery_images_upload_zip() {
	return drupal_get_form('one_flash_gallery_images_upload_zip_form');
}

function one_flash_gallery_images_upload_ftp() {
	return drupal_get_form('one_flash_gallery_images_upload_ftp_form');
}

function one_flash_gallery_images_upload_url() {
	return drupal_get_form('one_flash_gallery_images_upload_url_form');
}

function one_flash_gallery_images_upload_local_form() {
drupal_add_css(drupal_get_path('module', 'one_flash_gallery') .'/css/uploadify.css','module','all',FALSE);
drupal_add_js(drupal_get_path('module', 'one_flash_gallery') .'/js/uploadify.js', 'file');
drupal_add_js(drupal_get_path('module', 'one_flash_gallery') .'/js/fgallery_upload.js', 'file');
drupal_add_js(drupal_get_path('module', 'swfobject_api') .'/swfobject.js', 'file');
$module_path = base_path().drupal_get_path('module', 'one_flash_gallery');
$fgallery_upload = "jQuery(document).ready(function() {
	jQuery('#edit-uploadify').uploadify({
		'uploader'       : '".$module_path."/swf/uploadify.swf',
		'script'         : '".base_path()."admin/1-flash-gallery/uploadify',
		'cancelImg'      : '".$module_path."/images/cancel.png',
		'queueID'        : 'fileQueue',
		'fileDesc'       : 'Images',
		'folder'         : '".base_path().file_directory_path()."/fgallery',
		'fileExt'        : '*.jpg;*.jpeg;*.gif;*.bmp;*.png',
		'auto'           : false,
		'multi'          : true,
		'removeCompleted': true,
		'onComplete'     : function(event, queueID, fileObj, response, data){
				var array = response.split(\"___\");
				var id = \"image_\"+array[1];
				jQuery('#uploaded_images').append('<div class=\"uploaded_image '+id+'\">'+array[0]+'<p>'+fileObj.name+'<\/p><a href=\"javascript:void(0)\" rel=\"admin.php?page=fgallery_images&amp;action=delete&amp;id='+array[1]+'\" id=\"'+id+'\" title=\"Delete\" class=\"fgallery_action delete\" onclick=\"delete_img('+array[1]+');\">Delete<\/a><\/div>');
							},
		'onAllComplete'  : function () {
				jQuery('#uploaded_images').append('<br clear=\"all\" \/><a href=\"admin.php?page=fgallery_images&amp;folder='+jQuery('#edit-uploadify-img-folder-wrapper').val()+'\" class=\"fgallery_action\">Save<\/a>');
		}
	});
});";
drupal_add_js($fgallery_upload, 'inline', 'footer');
	$form = array();
	$folders = one_flash_gallery_get_folders();
	
	$form['uploadify_img_folder'] = array(
		'#type' => 'select', 
		'#title' => t('Images will be saved to'), 
		'#default_value' => 0,
		'#options' => $folders,
	);
	$form['uploadify'] = array(
		'#type' => 'file',  
		'#size' => 40,
		'#required' => TRUE,
		'#prefix' => '<div id="fileQueue"></div>',
		'#suffix' => '<p><a id="cancel" href="javascript:void(0);">'.t('Cancel all uploads').'</a> <a id="upload">'.t('Upload').'</a></p><div class="form_left" id="uploaded_images"></div>',
	);
	
	return $form;
}

function one_flash_gallery_images_upload_ftp_form() {	
	$form = array();
	$folders = one_flash_gallery_get_folders();
	
	$form['fgallery_url_folder'] = array(
		'#type' => 'select', 
		'#title' => t('Images will be saved to'), 
		'#default_value' => 0,
		'#options' => $folders,
	);
	
	$form['ftp_name'] = array(
		'#type' => 'textfield', 
		'#title' => t('FTP server name'), 
		'#size' => 60, 
		'#maxlength' => 128, 
		'#required' => TRUE,
	);
	
	$form['ftp_folder'] = array(
		'#type' => 'textfield', 
		'#title' => t('FTP Folder name'), 
		'#size' => 60, 
		'#maxlength' => 128, 
		'#required' => TRUE,
	);
	
	$form['ftp_username'] = array(
		'#type' => 'textfield', 
		'#title' => t('FTP Username'), 
		'#size' => 60, 
		'#maxlength' => 128, 
		'#required' => TRUE,
	);
	
	$form['ftp_pass'] = array(
	  '#type' => 'password', 
	  '#title' => t('FTP Password'), 
	  '#maxlength' => 64, 
	  '#size' => 15,
	);
	
	$form['fgallery_ftp_subfolders'] = array(
	  '#type' => 'checkbox', 
	  '#title' => t('Include Subfolders'),
	);
	
	$form['buttons']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Upload'),
	);
	
	return $form;
}

function one_flash_gallery_images_upload_ftp_form_submit($form,&$form_state) {	
global $filename;
		$ftp_name = $form_state['values']['ftp_name'];
		$ftp_user = $form_state['values']['ftp_username'];
		$ftp_pass = $form_state['values']['ftp_pass'];
		$ftp_url_folder = $form_state['values']['fgallery_url_folder'];
		$connect = ftp_connect($ftp_name) or die("Couldn't connect to $ftp_name");
		if ($ftp_pass !='') {
			if (!@ftp_login($connect, $ftp_user, $ftp_pass)) {
				die("Couldn't login to $ftp_name");
			}
		}
		$ftp_folder = $form_state['values']['ftp_folder'];
		$rec = $form_state['values']['fgallery_ftp_subfolders'];
		ftp_chdir($connect, $ftp_folder);
		// enabling passive mode
		ftp_pasv( $connect, true );
		// get contents of the current directory
		scan_ftp($connect, $ftp_folder, $rec); 
		$result = process_ftp($filename,$ftp_url_folder,$connect);
		ftp_close($connect);
	drupal_set_message(sprintf(t('%d image(s) were added to the list'),$i));
}

function one_flash_gallery_images_upload_url_form() {
drupal_add_css(drupal_get_path('module', 'one_flash_gallery') .'/css/fgallery.css','module','all',FALSE);
drupal_add_js(drupal_get_path('module', 'one_flash_gallery') .'/js/fgallery_upload.js', 'file');
	$form = array();
	$folders = one_flash_gallery_get_folders();
	
	$form['fgallery_url_folder'] = array(
		'#type' => 'select', 
		'#title' => t('Images will be saved to'), 
		'#default_value' => 0,
		'#options' => $folders,
	);
	$form['fgallery_url'] = array(
		'#type' => 'textfield', 
		'#title' => t('Image URL'), 
		'#attributes' => array('class' => 'fgallery_url', 'onchange'=>'show_img_from_url(this.value,0)'),
		'#size' => 60, 
		'#maxlength' => 128, 
		'#required' => TRUE,
		'#prefix' => '<a class="add_url" title="Add url"></a> <br />',
		'#field_suffix' => t('<img src="" id="fgallery_img_0" width="100"/><a class="delete_url" onclick="delete_url(0)" title="Delete url"></a><span class="clear"></span>')
	);
	$form['buttons']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Upload'),
	);
	
	return $form;
}

function one_flash_gallery_images_upload_url_form_submit($form, &$form_state){
	$post = $form['#post'];
	$urls = $post['fgallery_url_more'];
	$urls[] = $post['fgallery_url'];
	if (!empty($urls)) {
		$i = 0;
		@set_time_limit(0);
		foreach ($urls as $url) {
			$info = parse_url($url);
			$file_pathinfo = pathinfo($info['path']);
			$file_name = $_SERVER['DOCUMENT_ROOT']. base_path(). file_directory_path().'/fgallery/'.date("YmdHis").rand(0,100).'.'.$file_pathinfo['extension'];
			$fp = fopen($file_name, 'w');
			fwrite($fp, file_get_contents($url));
			fclose($fp); 
			$fileinfo = getimagesize($file_name);
			$img_type = $fileinfo['mime'];
			if (strpos($img_type,'image') === false) {
				echo sprintf(__('%s is not an image'),$url).'<br />';
				unlink($file_name);
				continue;
			}
			$img_path = str_replace($_SERVER['DOCUMENT_ROOT']. base_path(),'',$file_name);
			$size = filesize($file_name);
			db_query("INSERT INTO {fgallery_images} (img_caption, img_vs_folder, img_parent, img_date, img_type, img_size, img_path) VALUES('%s', %d, %d, NOW(), '%s', %d, '%s')", t('Image from URL'), 0, $post['fgallery_url_folder'], $img_type, $size, $img_path);
			$i++;
		}
	}
	drupal_set_message(sprintf(t('%d image(s) were added to the list'),$i));
}

function one_flash_gallery_images_upload_zip_form() {
	$form = array();
	$folders = one_flash_gallery_get_folders();
	
	$form['fgallery_url_folder'] = array(
		'#type' => 'select', 
		'#title' => t('Images will be saved to'), 
		'#default_value' => 0,
		'#options' => $folders,
	);
	$form['fgallery_zip'] = array(
		'#type' => 'file', 
		'#title' => t('Attach file'), 
		'#size' => 40,
		'#required' => TRUE,
	);
	$form['buttons']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Upload'),
	);
	
	return $form;
}

function one_flash_gallery_images_upload_zip_form_submit($form,&$form_state) {	
	if (!empty($_FILES)) {
		$zip_folder = $form_state['values']['fgallery_url_folder'];
		$i = fgallery_process_zip($_FILES, $zip_folder);
	}
	drupal_set_message(sprintf(t('%d image(s) were added to the list'),$i));
}

function one_flash_gallery_uploadify() {
	if (!empty($_FILES)) {
		$tempFile = $_FILES['Filedata']['tmp_name'];
		$targetPath = $_SERVER['DOCUMENT_ROOT']. base_path(). file_directory_path().'/fgallery/';
		$file_name = $_FILES['Filedata']['name'];
		$file_pathinfo = pathinfo($file_name);
		$filename = str_replace('.'.$file_pathinfo['extension'],'',$file_name);
		$file_name = date("YmdHis").'.'.$file_pathinfo['extension'];
		$targetFile =  str_replace('//','/',$targetPath) . $file_name;
		
		 $fileTypes  = str_replace('*.','',$_REQUEST['fileext']);
		 $fileTypes  = str_replace(';','|',$fileTypes);
		 $typesArray = split('\|',$fileTypes);
		 $fileParts  = pathinfo($_FILES['Filedata']['name']);
		
		 if (in_array(strtolower($fileParts['extension']),$typesArray)) {
			// Uncomment the following line if you want to make the directory if it doesn't exist
			// mkdir(str_replace('//','/',$targetPath), 0755, true);
			
			if (move_uploaded_file($tempFile,$targetFile)) {
				$file = getimagesize($targetFile);
				$img_type = $file['mime'];
				$img_path = str_replace($_SERVER['DOCUMENT_ROOT']. base_path(),'',$targetFile);
				$img_size = $_FILES['Filedata']['size'];
				db_query("INSERT INTO {fgallery_images} (img_caption, img_vs_folder, img_parent, img_date, img_type, img_size, img_path) VALUES('%s', %d, %d, NOW(), '%s', %d, '%s')",$filename, 0, $_REQUEST['img_parent'], $img_type, $img_size, $img_path);
			}
			
			   echo theme('imagecache', 'fgallery_cover', $img_path, $img_caption, $img_caption).'___'.$img_id;
		 } else {
			echo 'Invalid file type.';
		 }
}
}

// extract archive to temp dir
function fgallery_process_zip($data, $folder) {
	if($data['fgallery_zip']['tmp_name']['size'] && $data['fgallery_zip']['type'] == 'application/zip'){
        $tmp_fname = substr(md5($time), 0, 10) . '.zip';
        //move_uploaded_file($data['fgallery_zip']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].base_path().file_directory_path(). 'fgallery/tmp/'. $tmp_fname);
		/*file_save_uploaded('fgallery_zip',array(),file_directory_path().'fgallery/tmp/'.$tmp_fname);
        $filename = file_directory_path(). 'fgallery/tmp/'. $tmp_fname;
		if (!file_check_directory($filename. '_dir', FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
			die();
		}*/
		/*die();
        if(!unzip_file($filename, $filename. '_dir')){
			unlink($filename);
			continue;
        }   */
        unlink($filename);
        $uploaded_a_file = fgallery_process_directory($filename. '_dir', $_SERVER['DOCUMENT_ROOT'].base_path().file_directory_path().'fgallery', $folder);
        _one_flash_gallery_recursive_delete($filename. '_dir');
        unset($data['fgallery_zip']);
		return $uploaded_a_file;
    }
}

// process temp directory and insert images to db
function fgallery_process_directory($dir, $fgallery_dir, $folder){     
    $items = list_files($dir);
    $i = 0;
    foreach($items as $item){
		$file_name = str_replace($dir.'/', '',$item);
		$file_name = str_replace(' ','_',$file_name);
		$file_name = strtolower($file_name);
		$file_pathinfo = pathinfo($file_name);
		$filename = str_replace('.'.$file_pathinfo['extension'],'',$file_name);
		$file_name = date("YmdHis").rand(0,100).'.'.$file_pathinfo['extension'];
		
		$newitem = $fgallery_dir . '/'. $file_name;
		if (copy($item, $newitem)){
			$fileinfo = getimagesize($newitem);
			$img_type = $fileinfo['mime'];
			$img_path = str_replace($_SERVER['DOCUMENT_ROOT'].base_path(),'',$newitem);
			$img_size = filesize($newitem);
				db_query("INSERT INTO {fgallery_images} (img_caption, img_vs_folder, img_parent, img_date, img_type, img_size, img_path) VALUES('%s', %d, %d, NOW(), '%s', %d, '%s')", '', 0, $folder, $img_type, $img_size, $img_path);
			$i++;
		}
    }
    return $i;
}

/*
 * scan all files on FTP server route $link.$dir
 * $rec - include subfolders 
 * the results are collected into $filename global array
 */
function scan_ftp($link, $dir, $rec) { 
    global $filename; 
    $file_list = ftp_rawlist($link, $dir); 
    foreach($file_list as $file) { 
      list($acc, 
           $bloks, 
           $group, 
           $user, 
           $size, 
           $month, 
           $day, 
           $year, 
           $file) = preg_split("/[\s]+/", $file); 
      if(substr($file, 0, 1) == '.') continue; 
      if(substr($acc, 0, 1) == 'd') { 
        if ($rec) scan_ftp($link, $dir.$file."/", $rec); 
      } 
      if(substr($acc, 0, 1) == '-') { 
        $filename[] = $dir.$file; 
      } 
    } 
  } 
  
  /*
   * getting files from ftp to server and inserting info into db
   */
  function process_ftp($array, $ftp_folder, $conn_id) {
	global $wpdb;
	@set_time_limit(0);
	$error = '';
	$i = 0;
	if (!empty($array)) {
		foreach ($array as $key=>$value) {
			$file_parts = pathinfo($value);
			$targetFile = $_SERVER['DOCUMENT_ROOT']. base_path().file_directory_path().'/fgallery/'.date("YmdHsi").mt_rand(0,10).'.'.$file_parts['extension'];
			$handle = fopen($targetFile, 'w');
			if (@ftp_fget($conn_id, $handle, $value, FTP_BINARY, 0)) {
			 sleep(500);
			} else {
			 $error .= "There was a problem while downloading $value to $targetFile\n";
			}
			fclose($handle);
			$file = @getimagesize($targetFile);
			if (strpos($file['mime'],'image') === false) {
				$error .= "$targetFile is not an image file";
				unlink($targetFile);
			} else {
				$i++;
				$img_size = filesize($targetFile);
				$img_path = str_replace($_SERVER['DOCUMENT_ROOT']. base_path(),'',$targetFile);
				db_query("INSERT INTO {fgallery_images} (img_caption, img_vs_folder, img_parent, img_date, img_type, img_size, img_path) VALUES('%s', %d, %d, NOW(), '%s', %d, '%s')",$file_parts['basename'], 0, $ftp_folder, $file['mime'], $img_size, $img_path);
			}
		}
	}
	if ($error == '') {
		$error = $i;
	}
	return $error;
  }

?>