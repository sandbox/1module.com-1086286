<?php 

function one_flash_gallery_images($gallery) {
if (!empty($gallery)) {
    $gall_id = $gallery->gall_id;
	$images = one_flash_gallery_get_images_from_gallery($gall_id);
	header("Content-type: text/xml");
	echo "<?xml version='1.0' encoding='UTF-8'?>";
	echo '<images>';
		$siteurl = base_path();
		$nofolder = '';
		if (!empty($images)) :
			foreach ($images as $image){
				if ($image->img_vs_folder == 2) {
					$rows = one_flash_gallery_get_images_from_album($image->img_id, $gallery->gall_id);
					if (count($rows) > 0) {
						echo '<folder name="'.$image->img_caption.'">';
						foreach ($rows as $row) {
							if (in_array($gallery->gall_type,array(0,1,2,3))) {
								echo '<img file="'.$siteurl.$row->img_path.'" title="'.$row->img_caption.'" />'; 
							} else {
								echo '<img file="'.$siteurl.$row->img_path.'" title="'.$row->img_caption.'"><![CDATA['.$row->img_description.']]></img>'; 
							}
						}
						echo "</folder>";
					}
				} else {
					if (in_array($gallery->gall_type,array(0,1,2,3))) {
							$nofolder .= '<img file="'.$siteurl.$image->img_path.'" title="'.$image->img_caption.'" />';
						} else {
							$nofolder .= '<img file="'.$siteurl.$image->img_path.'" title="'.$image->img_caption.'"><![CDATA['.$image->img_description.']]></img>';
						}
				}
			}
			if ($nofolder !='') {
				$nofolder = '<folder name="'.$gallery->gall_name.'">'.$nofolder.'</folder>';
				echo $nofolder;
			}
		endif;
		echo '</images>';
} else {
    drupal_set_message(t('Unknown Gallery.'), 'error');
	drupal_goto('admin/1-flash-gallery/galleries');
}

die();
}

?>